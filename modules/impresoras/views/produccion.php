<?php
$this->load->view('../../themes/default/header');
$this->load->helper('form');
?>

<div class="pagetitle">
    <h1>Produccion Maquina: <?php echo $maquina ?></h1> 
    <span>Carga de Actual: <b><?php echo $listaProduccion['totalM'] ?></b> minutos</span>
</div><!--pagetitle-->


<div class="maincontent">
    <div class="contentinner">

        <!--Produccion Actual-->
        <h4 class="widgettitle ctitle">Producciendo</h4>

        <div class="row-fluid">

            <div class="span4">
                <h4 class="widgettitle">Numero de Parte</h4>
                <div class="widgetcontent">
                    <p style="text-align:center;"> <?php echo $listaProduccion['n_parte'][0]; ?> </p>
                </div><!--widgetcontent-->
            </div><!--span4-->

            <div class="span4">
                <div class="span6">
                    <h4 class="widgettitle">C Programada</h4>
                    <div class="widgetcontent">
                        <p style="text-align:center;"> <?php echo $listaProduccion['total_prod'][0]; ?> </p>
                    </div><!--widgetcontent-->
                </div>
                <div class="span6">
                    <h4 class="widgettitle">C Acumulada</h4>
                    <div class="widgetcontent">
                        <p style="text-align:center;"> <?php echo $listaProduccion['cantidad'][0]; ?> </p>
                    </div><!--widgetcontent-->
                </div>
            </div><!--span4-->

            <div class="span4">
                <div class="span6">
                    <h4 class="widgettitle">Color Primario</h4>
                    <p style="text-align:center;"> <?php echo $listaProduccion['tinta_p'][0]; ?> </p>
                </div>
                <div class="span6">
                    <h4 class="widgettitle">Color Secundario</h4>
                    <p style="text-align:center;"> <?php echo $listaProduccion['tinta_s'][0]; ?> </p>
                </div>
            </div><!--span4-->

        </div><!--row-fluid-->
        <form action="<?php echo $base; ?>impresoras/move/" method="post" accept-charset="utf-8">
            <div style="display:none">
                <input type="hidden" name="n_parte" value="<?php echo $listaProduccion['n_parte'][0] ?>" />
                <input type="hidden" name="mov_id" value="<?php echo $listaProduccion['mov_id'][0] ?>" />
                <input type="hidden" name="total_prod" value="<?php echo $listaProduccion['total_prod'][0] ?>" />
                <input type="hidden" name="cantidad" value="<?php echo $listaProduccion['cantidad'][0] ?>" />
                <input type="hidden" name="proceso" value="<?php echo $listaProduccion['proceso'][0] ?>" />
            </div>
             <p class="stdformbutton" style="margin-top: 0px; margin-left: 80%;">
                <button class="btn btn-primary">Fin de Corrida</button>
            </p>
            
        </form>

        <div class="clearfix"></div>
        <!-- END Produccion Actual-->

        <h4 class="widgettitle ctitle">Lista de Produccion</h4>
        <table class="table table-bordered" id="dyntable">
            <colgroup>
                <col class="con0" style="align: center; width: 4%" />
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
            </colgroup>
            <thead>
                <tr>
                    <th>Numero de Parte</th>
                    <th>Cantidad</th>
                    <th>Ancho</th>
                    <th>Minutos</th>
                    <th>Color P</th>
                    <th>Color S</th>
                </tr>
            </thead>
            <tbody>
                <?php for ($i=1; $i < count($listaProduccion['n_parte']) ; $i++): ?>
                    <tr>
                        <td><?php echo $listaProduccion['n_parte'][$i]; ?></td>
                        <td><?php echo $listaProduccion['total_prod'][$i]; ?></td>
                        <td><?php echo $listaProduccion['ancho'][$i]; ?></td>
                        <td><?php echo $listaProduccion['stdrate'][$i]; ?></td>
                        <td><?php echo $listaProduccion['tinta_p'][$i]; ?></td>
                        <td><?php echo $listaProduccion['tinta_s'][$i]; ?></td>
                    </tr>
                <?php endfor ?>
            </tbody>
        </table>

    </div>
</div>

<?php $this->load->view('../../themes/default/fooder'); ?>
