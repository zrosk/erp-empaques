<?php
$this->load->view('../../themes/default/header');
$this->load->helper('form');
?>

<div class="pagetitle">
    <h1>Produccion Maquina: </h1> <span>Selecciona tu equipo de trabajo</span>
</div><!--pagetitle-->


<div class="maincontent">
    <div class="contentinner">

        <h4 class="widgettitle ctitle">Finalizar corrida</h4>

        <h4 class="widgettitle nomargin shadowed">Form Bordered</h4>
        <div class="widgetcontent bordered shadowed nopadding">
            <form action="<?php echo $base; ?>impresoras/fincorrida/" method="post" accept-charset="utf-8" id="form_move" class="stdform stdform2">
                <div style="display:none">
                    <input type="hidden" name="mov_id" value="<?php echo @$mov_id ?>" />
                    <input type="hidden" name="proceso_id" value="<?php echo $proceso['procesos_id'] ?>" />
                </div>
                <p>
                    <label>Numero de Parte</label>
                    <span class="field">
                        <input class="uniformselect" type="text" name="n_parte"  value="<?php echo $n_parte ?>"  readonly/>
                    </span>
                </p>
                <p>
                    <label>Cantidad</label>
                    <span class="field">
                        <input type="text" name="cantidad" value="<?php echo $total_prod ?>" />
                    </span>
                </p>
                <p>
                    <label>Material Rechazado</label>
                    <span class="field">
                        <input type="text" name="rechazado" value="" />
                    </span>
                </p>
                <p>
                    <label>Siguiente proceso</label>
                    <span class="field">
                        <span class="help-inline"><?php echo $proceso['nombre'] ?></span>
                    </span>
                </p>
                <p class="stdformbutton">
                    <button class="btn btn-primary">Terminar</button>
                </p>
            </form>
        </div>

    </div>
</div>


<?php $this->load->view('../../themes/default/fooder'); ?>