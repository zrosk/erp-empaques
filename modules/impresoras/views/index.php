<?php
$this->load->view('../../themes/default/header');
$this->load->helper('form');
?>

<div class="pagetitle">
    <h1>Selecciona una Maquina</h1> <span>Selecciona tu equipo de trabajo</span>
</div><!--pagetitle-->

<div class="maincontent">
    <div class="contentinner">
        <div class="widgetcontent bordered shadowed nopadding">  
            <?php $attributes = array('class' => 'stdform stdform2') ?>
            <?php echo form_open(base_url() . 'impresoras/checkmq', $attributes) ?>
            <p>
                <label>Select</label>
                <span class="field">
                    <?php echo form_dropdown('maquina', $mq, '', 'id="selection2" class="uniformselect"') ?>

                </span>
            </p>
            <p class="stdformbutton">
                <button class="btn btn-primary">Entrar</button>
            </p>
            <?php echo form_close(); ?>
        </div><!--widgetcontent-->
    </div><!--contentinner-->
</div><!--maincontent-->



<?php $this->load->view('../../themes/default/fooder'); ?>
