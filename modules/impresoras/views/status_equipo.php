
<?php $this->load->view('../../themes/default/header'); ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="<?php echo base_url() . 'themes/default/js/jquery.livequery.js' ?>"></script>
<script src="<?php echo base_url() . 'themes/default/js/jquery.colorbox-min.js' ?>"></script>


<div class="pagetitle">
    <h1>Programacion de Impresoras</h1>
</div><!--pagetitle-->


<div class="maincontent">
    <div class="contentinner">

        <h4 class="widgettitle nomargin shadowed">Maquinas</h4>
        <div class="widgetcontent bordered shadowed">
            <?php echo form_open(base_url() . 'impresoras/statusequipo'); ?>
            <p>
                <label>Maquina: </label>
                <span>
                    <?php echo form_dropdown('maquina', $mq, '', 'id="selection2" class="uniformselect"') ?>
                </span>
            </p>
            <?php echo form_close(); ?>
        </div>

        <table class="table table-bordered" id="listaProduccion">
            <colgroup>
                <col class="con0" style="align: center;"/>
                <col class="con0" style="align: center;"/>
                <col class="con0" style="align: center;"/>
                <col class="con0" style="align: center;"/>
                <col class="con0" style="align: center;"/>
                <col class="con0" style="align: center;"/>
            </colgroup>
            <thead>
                <tr>
                    <th></th>
                    <th>Numero de Parte</th>
                    <th>Cantidad Programada</th>
                    <th>Cantidad Producida</th>
                    <th>Prioridad</th>
                    <th>Orden</th>
                    <th>Color P</th>
                    <th>Color S</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="8" style="text-align: center;">Selecciona una Maquina</td>
                </tr>
            </tbody>
        </table>
        

        <div class="clearfix"></div>
    </div><!--contentinner-->
</div><!--maincontent-->

<script type="text/javascript">
    jQuery(document).ready(function() {

       
        
   
        $("#selection2").livequery("change",function() {
            var maquinaId = $(this).val();
            var ID;
            var priority;
            var orden;
            var mov_id;



            var post = $.post("<?php base_url() ?>impresoras/statusEquipo", {maquina: maquinaId});
            post.done(function(response) {
                $("#listaProduccion tbody > tr").remove();
                $("#listaProduccion tbody").append(response);
                $(".editbox").hide();
                $(".inline").colorbox();
            });
            $(".inline").colorbox();

        });


        $("span.result").livequery("click", function() {
            ID = $(this).parent().parent().attr("id");
            mov_id = $(this).parent().parent().attr("mov");


            $("#priority_" + ID).hide();
            $("#orden_" + ID).hide();
            $("#priority_input_" + ID).show();
            $("#orden_input_" + ID).show();


        });
        

        $(".editbox").livequery("change", function(e) {

            priority = $("#priority_input_" + ID).val();
            orden = $("#orden_input_" + ID).val();

            if (priority.length === 1 && orden.length === 1) {
                post = $.post("<?php base_url() ?>impresoras/updatestatus", {mov_id: mov_id, priority: priority, orden: orden});
                post.done(function() {
                    switch (priority) {
                        case "1":
                            priority = "Muy Urgente";
                            break;
                        case "2":
                            priority = "Urgente";
                            break;
                        default:
                        case "5":
                            priority = "Normal";
                            break;
                        case "7":
                            priority = "Baja";
                            break;
                    }
                    $("#priority_input_" + ID).hide();
                    $("#orden_input_" + ID).hide();

                    $("#priority_" + ID).text(priority).show();
                    $("#orden_" + ID).text(orden).show();

                });
            }
        });

        $(document).focusout(function() {
            $(".editbox").hide();
            $("span").show();
        });
        
         


    });
</script>
<?php $this->load->view('../../themes/default/fooder'); ?>
