
<div style='padding:10px; background:#fff;'>
    <h1>Cambio <?php echo $maquinas['now_parte'] ?> </h1>
    <div id="maquina_st"><p></p></div>
    <?php
    $return = "";
    for ($i = 0; $i < (count($maquinas) - 2); $i++) :
        $check = '';
        if ($maquinas['now_id'] == $maquinas[$i]['maquina']):
            $check = 'checked="checked"';
        endif;
        ?>

        <input type="radio" name="maquina" value="<?php echo $maquinas[$i]['maquina']; ?>" <?php echo @$check ?> >
        <?php echo $maquinas[$i]['nombre']; ?>
        <br>
    <?php endfor; ?>

    <p class="stdformbutton" style="margin-top: 0px; margin-left: 80%;">
        <button id="cambio" class="btn btn-primary">Cambio Maquina</button>
    </p>

</div>

<script type="text/javascript">
    $("#cambio").click(function() {
        var mq_id = $("input[name=maquina]:checked").val();
        var mov_id = "<?php echo @$mov_id ?>";
        var mq_act = <?php echo $maquinas['now_id'] ?>;

        if (mq_id != mq_act) {
            var post = $.post("<?php base_url() ?>impresoras/changemq", {mov_id: mov_id, mq_id: mq_id});
            post.done(function() {
                $("#maquina_st p").html('Cambio Exitoso');
                $("table tr[mov=" + mov_id + "]").remove();
            });

        }
    });

</script>
