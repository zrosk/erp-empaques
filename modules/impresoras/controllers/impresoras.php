<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Impresoras extends MX_Controller
{

    private $_data = array();
    private $_impModel;

    public function __construct()
    {
        parent::__construct();
        $this->_impModel = $this->load->model('imp_model');
        $this->_data['base'] = $this->config->base_url();

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    public function index()
    {
        $this->_data['mq'] = $this->imp_model->getListaMaquinas();
        $this->load->view('index', $this->_data);
    }

    public function produccion()
    {
        $id_maquina = $this->session->userdata('id_maquina');

        if (empty($id_maquina)) {
            redirect(base_url() . 'impresoras');
        }
        else {

            $this->_data['maquina'] = $this->session->userdata('n_maquina');
            $this->_data['listaProduccion'] = $this->imp_model->getProduccion2($id_maquina);

            if (empty($this->_data['listaProduccion'])) {
                echo 'Maquina no tiene carga <br>';
                echo anchor(base_url() . 'impresoras/', 'regresar');
                exit;
            }


            $this->load->view('produccion', $this->_data);
        }
    }

    public function checkmq()
    {
        $this->form_validation->set_rules('maquina', 'Seleccion Maquina', 'required');
        if ($this->form_validation->run() === FALSE) {
            redirect('impresoras/');
        }
        else {
            $maquina = explode('-', $this->input->post('maquina'));
            $ses = array(
            'n_maquina' => $maquina[1],
            'id_maquina' => $maquina[0]
            );
            $this->session->set_userdata($ses);
            redirect(base_url() . 'impresoras/produccion');
        }
    }

    public function move()
    {
        $this->form_validation->set_rules('mov_id', 'Error en Movimiento', 'required');
        if ($this->form_validation->run() === FALSE) {
            redirect(base_url() . 'impresoras/produccion');
            exit;
        }
        else {
            $this->_data['mov_id'] = $this->input->post('mov_id');
            $this->_data['n_parte'] = $this->input->post('n_parte');
            $this->_data['total_prod'] = $this->input->post('total_prod');
            $this->_data['cantidad'] = $this->input->post('cantidad');
            /*
              foreach ($this->input->post(NULL, TRUE) as $key=>$value){
              $this->_data["$key"] = $value;
              }
             */
            $this->_data['proceso'] = $this->imp_model->sigProceso($this->input->post('n_parte'), $this->input->post('proceso'));

            if ($this->_data['proceso'] == false) {
                $this->_data['proceso']['procesos_id'] = $this->input->post('proceso') + 1;
                $this->_data['proceso']['nombre'] = 'N/A';
            }
            $this->load->view('move', $this->_data);
        }
    }

    public function fincorrida()
    {
        $this->form_validation->set_rules('n_parte', 'Movimiento', 'required');
        $this->form_validation->set_rules('cantidad', 'Cantidad', 'required|numeric');
        $this->form_validation->set_rules('rechazado', 'Rechazo', 'required|numeric');

        $this->form_validation->set_message('required', 'Campo %s es requerido');

        if ($this->form_validation->run() === FALSE) {
            redirect(base_url() . 'impresoras/produccion');
            exit;
        }
        else {

            $data = $this->input->post(NULL, TRUE);

            if ($data['cantidad'] > $data['rechazado']) {
                $data['cantidad'] -= $data['rechazado'];
            }
            else {
                redirect(base_url() . 'impresoras/produccion');
                exit;
            }
            
            $this->_cambioProceso($data);

            redirect('impresoras/produccion');
        }
    }

    public function statusEquipo()
    {
        $this->form_validation->set_rules('maquina', 'Maquina', 'required|trim');
        $this->form_validation->set_message('required', 'Campo %s requerido');

        if ($this->form_validation->run() === FALSE) {

            $data['mq'] = $this->imp_model->getListaMaquinas();
            $this->load->view('status_equipo', $data);
        }
        else {

            $maquina = explode('-', $this->input->post('maquina'));
            $maquinaId = $maquina[0];
            $listaMov = $this->imp_model->getProduccion2($maquinaId);

            $return = "";
            $prioridad = array(
            '1' => 'Muy Urgente',
            '2' => 'Urgente',
            '5' => 'Normal',
            '7' => 'Baja'
            );
            for ($i = 0; $i < count($listaMov['n_parte']); $i++) {
                $return .= "<tr id=\"{$i}\" mov=\"{$listaMov['mov_id'][$i]}\">" . PHP_EOL;
                $return .= "<td><a class='inline' href=\" " . base_url() . "impresoras/movemq/{$listaMov['mov_id'][$i]} \">move</a></td>" . PHP_EOL;
                $return .= "<td> {$listaMov['n_parte'][$i]} </td>" . PHP_EOL;
                $return .= "<td> {$listaMov['total_prod'][$i]} </td>" . PHP_EOL;
                $return .= "<td> {$listaMov['cantidad'][$i]} </td>" . PHP_EOL;
                $return .= "<td>" . PHP_EOL;
                $return .= "<span id='priority_$i' class ='result'> {$prioridad[$listaMov['priority'][$i]]} </span>" . PHP_EOL;
                $return .= "<select id='priority_input_$i' class='editbox' name='prioridad'>" . PHP_EOL;
                $return .= "<option " . (($listaMov['priority'][$i] == 7) ? "selected" : "") . " value='7'>Baja</option>" . PHP_EOL;
                $return .= "<option " . (($listaMov['priority'][$i] == 5) ? 'selected' : "") . " value='5'>Normal</option>" . PHP_EOL;
                $return .= "<option " . (($listaMov['priority'][$i] == 2) ? "selected" : "") . " value='2'>Urgente</option>" . PHP_EOL;
                $return .= "<option " . (($listaMov['priority'][$i] == 1) ? "selected" : "") . " value='1'>Muy Urgente</option>" . PHP_EOL;
                $return .= "</select>" . PHP_EOL;
                $return .= "</td>" . PHP_EOL;
                $return .= "<td>" . PHP_EOL;
                $return .= "<span id='orden_$i' class='result' > {$listaMov['orden'][$i]} </span>";
                $return .= "<input id='orden_input_$i' class='editbox' type='text' value='{$listaMov['orden'][$i]}' />" . PHP_EOL;
                $return .= "</td>" . PHP_EOL;
                $return .= "<td> {$listaMov['tinta_p'][$i]} </td>" . PHP_EOL;
                $return .= "<td> {$listaMov['tinta_s'][$i]} </td>" . PHP_EOL;
                $return .= "</tr>" . PHP_EOL;
            }
            echo $return;
        } //END form_validation
    }

    public function updateStatus()
    {
        $data = array(
        'priority' => $this->input->post('priority'),
        'orden' => $this->input->post('orden'),
        );
        $this->db->where('mov_id', $this->input->post('mov_id'));
        $this->db->update('movimientos', $data);
    }

    public function moveMq($mov_id)
    {
        $data['maquinas'] = $this->imp_model->getAltMq($mov_id);
        $data['mov_id'] = $mov_id;

        $this->load->view('maquina_alt_ajax', $data);
    }

    public function changeMq()
    {
        $update = array('maquina_id' => $this->input->post('mq_id'));
        $mov_id = $this->input->post('mov_id');

        $this->db->where('mov_id', $mov_id);
        $this->db->update('movimientos', $update);
    }

    private function _cambioProceso($data)
    {
        $procesoFinalid = '5';

        if ($data['proceso_id'] == $procesoFinalid) {
            $this->imp_model->delMov($data['mov_id']);
            $this->imp_model->toAlmacen($data);
        }
        else {
            $this->imp_model->cambioProceso($data);
            return true;
        }

        return false;
    }

}
