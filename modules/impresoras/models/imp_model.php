<?php

class Imp_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function getListaMaquinas()
    {
        $this->db->select('maquina_id,nombre');
        $this->db->order_by('nombre');
        $query = $this->db->get('maquina');

        if ($query->num_rows() > 0) {
            $result = array();
            foreach ($query->result() as $row) {
                /*
                 * @ id - nombre
                 * Evitar un Join hacia tabla maquina getproduccion()
                 */
                $result["$row->maquina_id-$row->nombre"] = $row->nombre;
            }
            return $result;
        }
        return array(array("maquina_id" => 0, "nombre" => "No se encontraron maquinas"));
    }

    private function _getRate($maquina_id)
    {
        $this->db->select('rate');
        $this->db->where('maquina_id', $maquina_id);
        $query = $this->db->get('maquina');
        if($query->num_rows > 1){
            $row = $query->row_array(0);
            return (int) $row['rate'];
        }
        else            
            return (int)77;
    }
    private function _getColor($n_parte)
    {
        $this->db->where('prod_tintas.n_parte', $n_parte);
        $this->db->from('prod_tintas');
        $this->db->join('tintas', 'prod_tintas.tinta_id = tintas.tinta_id', 'left');
        $this->db->order_by("nivel", "asc");
        $this->db->limit(2);
        $result = $this->db->get();
        
        if($result->num_rows > 0){
            return $result->result_array();
        }
        
        return false;
    }
    private function _getAncho($n_parte)
    {
        $this->db->select('ancho');
        $this->db->where('n_parte', $n_parte);
        $result = $this->db->get('productos', 1);
        if($result->num_rows > 0){
            return (int) $result->row()->ancho;
        }
        else {
            return 'N/A';
        }
    }

    public function getProduccion2($maquina_id)
    {
        $produccion = $this->db->where('maquina_id', $maquina_id)->get('movimientos');
        if ($produccion->num_rows < 1)
            return false;
        
        $rate = $this->_getRate($maquina_id); 
        $lista = $this->_produccionLista($produccion->result_array(), $rate);
        
        return $lista;
    }
    private function _produccionLista(array $produccion, $rate)
    {
        
        $totalMinutos = 0;
        $result = array();
        foreach ($produccion as $value) {
            
            $result['mov_id'][] = $value['mov_id'];
            $result['n_parte'][] = $value['n_parte'];
            $result['total_prod'][] = $value['total_prod'];
            $result['ct'][] = $value['ct'];
            $result['proceso'][] = $value['proceso'];
            $result['fecha'][] = $value['fecha'];
            $result['priority'][] = $value['priority'];
            $result['orden'][] = $value['orden'];
            $result['cantidad'][] = $value['cantidad'];
            $result['mk_i'][] = $value['mk_i'];
            $result['mk_f'][] = $value['mk_f'];
            
            $result['ancho'][] = @$this->_getAncho($value['n_parte']);
            
            $tinta = $this->_getColor($value['n_parte']);
            $result['tinta_p'][] = @$tinta[0]['codigo'];
            $result['tinta_s'][] = @$tinta[1]['codigo'];

            $stdrate = ceil((int) $value['total_prod'] / (int) $rate);
            $result['stdrate'][] = $stdrate;
            $totalMinutos += $stdrate;
        }
        array_multisort(
        $result['priority'], SORT_ASC,
        $result['orden'], SORT_ASC,
        $result['ancho'], SORT_DESC, 
        $result['tinta_p'], SORT_DESC,
        $result['tinta_s'], SORT_DESC,
        $result['mov_id'] ,
        $result['n_parte'] ,
        $result['total_prod'] ,
        $result['ct'] ,
        $result['proceso'] ,
        $result['fecha'] ,
        $result['cantidad'],
        $result['mk_i'], 
        $result['stdrate']
        );
        
        $result['totalM'] = $totalMinutos;
        return $result;
    }
    public function getProduccion3($maquina_id) {

        $this->db->select('
                            movimientos.mov_id, 
                            movimientos.n_parte,
                            movimientos.total_prod,
                            movimientos.fecha,
                            movimientos.priority,
                            movimientos.orden,
                            movimientos.cantidad,
                            productos.ancho,
                            tintas.codigo,
                            tintas.tinta_id
                         ');
        $this->db->from('movimientos');
        $this->db->join('productos', 'productos.n_parte = movimientos.n_parte','left');
        $this->db->join('prod_tintas', 'prod_tintas.n_parte = movimientos.n_parte', 'left');
        $this->db->join('tintas', 'tintas.tinta_id = prod_tintas.tinta_id', 'left');
        $this->db->where('movimientos.maquina_id', $maquina_id);
        $this->db->order_by('movimientos.priority ASC, movimientos.orden ASC, productos.ancho DESC, tintas.codigo ASC');;
        $result = $this->db->get();
        if($result->num_rows > 0)
            return $result->result_array();
        
        return false;
        
    }

    public function getProduccion($id)
    {

        $rate = $this->_getRate($id);

        $this->db->where('maquina_id', $id);
        $this->db->from('movimientos');
        $query = $this->db->get();
        if($query->num_rows < 1 )
            return false;

        $lista = $query->result_array();
        $query->free_result();
        $i = 0;
        $total_min = 0;
        foreach ($lista as $key) {
            //$this->db->where('prod_tintas.tinta_id','tintas.tinta_id');
            $this->db->where('prod_tintas.n_parte', $key['n_parte']);
            $this->db->from('prod_tintas');
            $this->db->join('tintas', 'prod_tintas.tinta_id = tintas.tinta_id', 'left');
            $this->db->order_by("nivel", "asc");
            $this->db->limit(2);
            $query2 = $this->db->get();
            $row = $query2->result_array();

            $result[$i]['mov_id'] = $key['mov_id'];
            $result[$i]['n_parte'] = $key['n_parte'];
            $result[$i]['total_prod'] = $key['total_prod'];
            $result[$i]['ct'] = $key['ct'];
            $result[$i]['proceso'] = $key['proceso'];
            $result[$i]['fecha'] = $key['fecha'];
            $result[$i]['priority'] = $key['priority'];
            $result[$i]['orden'] = $key['orden'];
            $result[$i]['cantidad'] = $key['cantidad'];
            $result[$i]['mk_i'] = $key['mk_i'];
            $result[$i]['mk_f'] = $key['mk_f'];
            $result[$i]['tinta_p'] = @$row[0]['codigo'];
            $result[$i]['tinta_s'] = @$row[1]['codigo'];

            $stdrate = ceil((int) $key['total_prod'] / (int) $rate);
            //$stdrate = $stdrate/60;
            $result[$i]['stdrate'] = $stdrate;
            $total_min += $stdrate;

            $i++;
        }
        return $result;
    }

    public function sigProceso($n_parte, $actual)
    {
        $this->db->select('procesos.procesos_id, procesos.nombre');
        $this->db->from('prod_procesos');
        $this->db->where('n_parte', $n_parte);
        $this->db->where('orden', $actual + 1);
        $this->db->join('procesos', 'procesos.procesos_id=prod_procesos.procesos_id', 'left');
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $row = $query->row_array();
            return $row;
        }
        else
            return false;
    }
    
    public function cambioProceso(Array $data)
    {
        $where = $data['mov_id'];
        $update = array(
            'proceso' => $data['proceso_id'],
            'total_prod' => $data['cantidad'],
            'cantidad' => 0,
            'maquina_id' => 0,
            'ct' => 0
        );
        
        $this->db->where('mov_id', $where);
        $this->db->update('movimientos', $update);
    }
    
    public function getAltMq($mov_id)
    {
        $query = $this->db->select('n_parte, maquina_id')->where('mov_id', $mov_id)->get('movimientos')->row();
        $n_parte = $query->n_parte;
        $maquia = $query->maquina_id;
        //$query->free_result();
        
        
        $query2 = $this->db->select('prod_maquina.maquina, maquina.nombre, prod_maquina.orden')
                           ->where('n_parte', $n_parte)
                           ->join('maquina', 'maquina.maquina_id = prod_maquina.maquina','left')
                           ->order_by('orden')->get('prod_maquina')->result_array();
        $query2['now_id'] = $maquia;
        $query2['now_parte'] = $n_parte;
  
        return $query2;
    }
    public function toAlmacen($data)
    {
        $exist = $this->db->where('n_parte', $data['n_parte'])->get('inventario')->num_rows;
        
        if ($exist > 0) {
            $this->db->set('cantidad', "`cantidad` + {$data['cantidad']}", FALSE);
            $this->db->where('n_parte', $data['n_parte']);
            $this->db->update('inventario');
            
        }
        else {
            $insert = array(
                'n_parte' => $data['n_parte'] , 
                'cantidad' => $data['cantidad']
            );
            $this->db->insert('inventario', $insert);
            
        }
        
        if ($this->db->affected_rows() > 0){
            return true;
        }
        return false;
    }
    public function delMov($mov_id)
    {
        $this->db->delete('movimientos', "mov_id = {$mov_id}");
    }

}