<?php

class Corr_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function getProgramaId($id)
    {
        $this->db->from('corrugadora');
        $this->db->where('n_programa', $id);
        $this->db->order_by('fila');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function insert($data)
    {
        $this->db->insert('corrugadora', $data);
    }

    public function toMovimientos($movimientos)
    {
        foreach ($movimientos as $key => $value) {

            $where = "n_parte = \"$key\" AND ct = '1500'";
            if ($this->db->get_where('movimientos', $where)->num_rows() > 0) {

                $this->db->set('total_prod', "`total_prod` + $value", false);
                $this->db->where('n_parte', $key);
                $this->db->where('ct', '1500');
                $this->db->update('movimientos');
            }
            else {
                /*
                 * ct,proceso se agregan por defecto en la DB 
                 * maquina 9 es corrugadora
                 */
                $insert = array(
                'n_parte' => $key,
                'total_prod' => $value,
                'fecha' => date("Y-m-d H:i:s"),
                'maquina_id' => 9,
                'proceso' => 0,
                'ct' => 1500
                );
                $this->db->insert('movimientos', $insert);
            }
        }
        if ($this->db->affected_rows())
            return true;
        return false;
    }

    private function _delFila($fila, $id)
    {
        $where = "n_programa = '$id' AND fila = '$fila'";
        if ($this->db->delete('corrugadora', $where, 2))
            return true;
        return false;
    }

    public function terminar($fila, $programaID)
    {
        $query = $this->db->select('n_parte,cantidad')
        ->where('fila', $fila)
        ->where('n_programa', $programaID)
        ->get('corrugadora', 2);

        if ($query->num_rows() < 1)
            return false;

        $programaFila = $query->result_array();
        $query->free_result();



        $this->db->trans_start();
        /*
         * Corrugadora
         */
        $where = "n_programa = '$programaID' AND fila = '$fila'";
        $this->db->delete('corrugadora', $where, 2);

        /*
         * Movimientos
         */
        foreach ($programaFila as $programa) {

            $numeroParte = $programa['n_parte'];
            $cantidad = $programa['cantidad'];
            $query = $this->db
            ->where('n_parte', $numeroParte)
            ->where('ct', 1500)
            ->where("`cantidad` + $cantidad >=", 'total_prod', false)
            ->get('movimientos');

            if ($query->num_rows > 0) {
                $sigProceso = $query->row_array();
                $this->_cambioProceso($sigProceso, $cantidad);
            }
            else {
                $this->db
                ->set('cantidad', `cantidad` + $cantidad, false)
                ->where('ct', '1500')
                ->where('n_parte', $key['n_parte'])
                ->update('movimientos');
            }
            $query->free_result();
        }

        $this->db->trans_complete();



        if ($this->db->trans_status() !== FALSE) {
            return true;
        }
        else {
            $this->db->trans_rollback();
        }

        return false;
    }

    private function _cambioProceso($sigProceso, $cantidad = 0)
    {
        $numeroParte = $sigProceso['n_parte'];
        $procesesoIdOrden = (int) $sigProceso['proceso'] + 1;
        $totalProd = $sigProceso['cantidad'] + $cantidad;

        $next = $this->db->select('procesos.procesos_id, procesos.nombre')
        ->join('prod_procesos', 'prod_procesos.procesos_id=procesos.procesos_id', 'left')
        ->where('n_parte', $numeroParte)
        ->where('orden', $procesesoIdOrden)
        ->get('procesos');

        if ($next->num_rows < 1) {
            $procesoId = 0;
            //Tirar error NO se encontro procesos
        }
        else {
            $result = $next->row_array();
            $procesoId = $result['procesos_id'];
            $procesoNombre = $result['nombre'];
        }
        $next->free_result();

        if ($procesoId == 2) {
            /*
             * Si proceso es IMPRESORAS
             * Seleccionar la maquina optima para su fabricacion
             */

            $queryMaquina = $this->db->select('maquina')
            ->where('n_parte', $numeroParte)
            ->where('orden', 1)
            ->get('prod_maquina', 1);

            if ($queryMaquina->num_rows() < 1) {
                $maquina = 1;
            }
            else {
                $maquina = $queryMaquina->row()->maquina;
            }
            $queryMaquia->free_result();


            $this->db
            ->set('ct', 2000)
            ->set('proceso', $procesesoIdOrden)
            ->set('total_prod', $totalProd)
            ->set('cantidad', 0)
            ->set('maquina_id', $maquina)
            ->where('mov_id', $sigProceso['mov_id'])
            ->update('movimientos');

            return true;
        }
        else {

            $this->db
            ->set('ct', 0)
            ->set('proceso', $procesesoIdOrden)
            ->set('total_prod', $totalProd)
            ->set('cantidad', 0)
            ->set('maquina_id', 99)
            ->where('mov_id', $sigProceso['mov_id'])
            ->update('movimientos');

            return true;
        }

        return false;
    }

}

