<?php $this->load->view('../../themes/default/header'); ?>

<div class="pagetitle">
    <h1>Corrugadora</h1><span>Teclea el numero de Programa a Correr</span>
</div><!--pagetitle-->

<div class="maincontent">
    <div class="contentinner">

        <h4 class="widgettitle ctitle">Programa</h4>
        <div class="widgetcontent">
            <div id="notice"><?php echo validation_errors(); ?></div>

            <?php echo form_open(base_url() . 'corrugadora/','class="stdform"'); ?>

            <p>
                <label>Numero de Programa: </label>
                <span class="filed">
                <?php echo form_input('n_programa',null,'class="input-medium"'); ?>
                </span>
            </p>


            <?php echo form_submit('Enviar', 'Programa'); ?>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<?php $this->load->view('../../themes/default/fooder'); ?>
