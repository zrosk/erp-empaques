
<?php $this->load->view('../../themes/default/header'); ?>

<div class="pagetitle">
    <h1>Corrugadora</h1>
    <span>Programa: <?php echo $lista[0]['n_programa']; ?></span>
    <span>Fecha: <?php $lista[0]['fecha']; ?></span>
</div><!--pagetitle-->

<div class="maincontent">
    <div class="contentinner">

        <h4 class="widgettitle ctitle">Listado del programa</h4>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Fila</th>
                    <th>Numero de parte</th>
                    <th>Cantidad</th>
                </tr>
            </thead>
            <tbody>
                <?php for ($i = 0; $i < count($lista); $i++) : ?>
                    <?php $numeroParte = $lista[$i]['n_parte']; ?>
                    <?php $cantidad = $lista[$i]['cantidad']; ?> 
                    <?php $fila = $lista[$i]['fila']; ?> 
                    <?php @$nextFila = $lista[$i + 1]['fila']; ?>
                    <tr id="<?= $fila ?>" class="fila_<?= $fila ?>">
                        <td><?= $fila ?></td>
                        <td><?= $numeroParte ?></td>
                        <td><?= $cantidad ?></td>
                    </tr>
                    <?php if ($fila != $nextFila): ?>
                        <tr id="<?= $fila ?>" class="fila_<?= $fila ?>" >
                            <td colspan="3" style="text-align: center">
                                <button id="terminar" class="btn btn-primary">Terminar</button>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endfor; ?>
            </tbody>
        </table>

    </div>
    <!--END of widget --->
</div>



<script type="text/javascript">

    jQuery(document).ready(function() {
        $('#terminar').click(function() {

            if (prompt('Escribe " ok " si la corrida ya termino', '') !== 'ok')
                return false;

            var fila = $(this).parent().parent().attr("id");            
            var programaId = <?php echo $lista[0]['n_programa'] ?>;
            var posting = $.post("<?php echo base_url() ?>corrugadora/terminar/", {file: fila, id: programaId});

            posting.done(function(data) {
                if (data === 'true') {
                    $(".fila_" + fila).toggle().remove();
                    return true;
                };
                return false;

            });

        });
    });

</script>

<?php $this->load->view('../../themes/default/fooder'); ?>

