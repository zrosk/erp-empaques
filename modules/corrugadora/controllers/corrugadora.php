<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Corrugadora extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('corr_model');
    }

    public function importarPrograma()
    {
        $this->load->view('import_excel');
    }

    public function index()
    {
        $this->form_validation->set_rules('n_programa', 'Numero de Programa no valido', 'required|integer');
        $this->form_validation->set_message('required','Campo %s no valido');
        
        $data['title'] = 'Corrugadora';

        if ($this->form_validation->run() === false) {
            $this->load->view('selectPrograma', $data);
        }
        else {

            $data['lista'] = $this->corr_model->getProgramaId($this->input->post('n_programa'));

            if (count($data['lista']) < 1) {
                redirect(base_url() . 'corrugadora');
                exit;
            }

            $this->load->view('litsadoPrograma', $data);
        }
    }

    public function doUpload()
    {
        $config['upload_path'] = './files/';
        $config['allowed_types'] = 'xls|xlsx|cvs';
        $config['overwrite'] = true;
        $config['file_name'] = 'importexc';
        $config['max_size'] = 10 * 1024;
        
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            
            $error = array('error' => $this->upload->display_errors());
            echo $this->upload->display_errors();
        }
        else {
            
            $excel = $this->upload->data();     
            $this->_procesarPrograma($excel['full_path']);
        }
    }

    private function _procesarPrograma($file)
    {
        $this->load->library('excel');
        $obj_excel = PHPExcel_IOFactory::load($file);
        $sheetData = $obj_excel->getActiveSheet()->toArray(null, true, true, true);

        $data = array();
        $impresoras = array();
        $date = date("Y-m-d H:i:s");
        foreach ($sheetData as $element) {
            //Remplazar con preg_match
            if (strpos($element['A'], '-') === false)
                continue;

            $n_parte = $this->fixParte($element['A']);
            $data = array(
                'n_parte' => $n_parte,
                'n_programa' => $element['C'],
                'cantidad' => $element['B'],
                'fila' => $element['D'],
                'fecha' => $date
            );
            //Save corrugadora
            $this->corr_model->insert($data);
            
            //To movimientos
            //Suma Todos numero parte iguales
            //@$impresoras["$n_parte"]['n_parte'] = $n_parte;
            //@$impresoras["$n_parte"]['total_prod'] += $element['B'];
            //@$impresoras["$n_parte"]['fecha'] = $date;
            @$impresoras["$n_parte"] += $element['B'];
        }
        
        //Save Movimientos
        $this->corr_model->toMovimientos($impresoras);

        
        echo anchor(base_url() . 'corrugadora/importarPrograma', 'Regresar');
        //redirect(base_url() . 'corrugadora/importarPrograma');
    }

    private function fixParte($nParte)
    {
        $n_parte = explode('-', $nParte);
        if (strlen($n_parte[0]) < 5) {
            $n_parte[0] = str_repeat('0', 5 - strlen($n_parte[0])) . $n_parte[0];
        }
        if (strlen($n_parte[1]) < 3) {
            $n_parte[1] = str_repeat('0', 3 - strlen($n_parte[1])) . $n_parte[1];
        }
        return implode('-', $n_parte);
    }

    public function terminar()
    {
        if ($this->input->is_ajax_request()) {
       
            if( $this->corr_model->terminar($this->input->post('file'), $this->input->post('id')) ){
                echo 'true';
                return true;
            }
            echo 'false';
            return false;
        }
    }
    
    public function subirdb($pach)
    {
        $this->load->library('excel');
        $this->load->database();
        $obj_excel = PHPExcel_IOFactory::load($pach);
        $sheetData = $obj_excel->getActiveSheet()->toArray(null, true, true, true);


        foreach ($sheetData as $element) {
            if (strpos($element['A'], '-') === false)
                continue;
            $nparte = trim($element['A']);
            $j = 1;
            foreach ($element as $value){
                if ($nparte == trim($value))
                    continue;
                $this->db->insert('prod_tintas',array('n_parte'=>$nparte, 'tinta_id'=>$value, 'nivel'=>$j));
                $j++;
                
            }
        }
    }

}

