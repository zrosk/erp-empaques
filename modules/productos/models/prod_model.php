<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class prod_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function listaProducto($limit = 1, $offset = 0,$filtro = '1=1')
    {
        $lista = $this->db->where($filtro,null,false)->limit($limit, $offset)->order_by('n_parte',' ASC')->get('productos');
        if($lista->num_rows() < 1 )
            return false;
        return $lista->result_array();
        
    }
    public function countAllProducto($filtro = 1)
    {
        $lista = $this->db->get('productos');
        return $lista->num_rows();
    }
    public function updateProducto($data)
    {
        $where = array_pop($data);
        $this->db->where('id', $where);
        $this->db->update('productos',$data);
    }
    public function getProcesos($n_parte)
    {
       $this->db->select('procesos.procesos_id, procesos.nombre');
       $this->db->from('procesos');
       $this->db->join('prod_procesos', 'prod_procesos.procesos_id = procesos.procesos_id', 'left');
       $this->db->where('prod_procesos.n_parte',$n_parte);
       //$this->db->where('prod_procesos.n_parte','00315-096');
       $this->db->order_by('orden','ASC');
       
       $query = $this->db->get();
       if($query->num_rows > 0){
           return $query->result_array();
       }
       return false;
    }
    
    public function getProcesosNotIn($n_parte)
    {
        $sql = "
                SELECT *
                FROM procesos
                WHERE procesos_id NOT
                IN (
                SELECT procesos.procesos_id
                FROM `procesos`
                JOIN prod_procesos ON prod_procesos.procesos_id = procesos.procesos_id
                WHERE n_parte = '{$n_parte}'
                )
                ";
        $query = $this->db->query($sql);
        if($query->num_rows > 0){
            return json_encode($query->result_array());
        }
        return FALSE;    
    }

    public function addProceso($nParte, $ProcesoId)
    {
        $orden = $this->db->select_max('orden')->where('n_parte', $nParte)->get('prod_procesos')->row()->orden;
        if(is_null($orden))
            $orden = 1;
        else
            $orden++;
        $insert = array(
                    'n_parte' => $nParte,
                    'orden' => $orden,
                    'procesos_id' => $ProcesoId,
                  );
        $this->db->insert('prod_procesos',$insert);
        
    }
    public function getTintas($n_parte)
    {
        $this->db->where('n_parte', $n_parte);
        $this->db->join('tintas', 'tintas.tinta_id = prod_tintas.tinta_id', 'left');
        $this->db->order_by('nivel', 'ASC');
        $tintas = $this->db->get('prod_tintas');
        
        if($tintas->num_rows > 0){
            return $tintas->result_array();
        }
        return false;
    }
    
      public function getTintaNotIn($n_parte)
    {
        $sql = "
                SELECT *
                FROM tintas
                WHERE tinta_id NOT
                IN (
                SELECT tintas.tinta_id
                FROM `tintas`
                JOIN prod_tintas ON prod_tintas.tinta_id = tintas.tinta_id
                WHERE n_parte = '{$n_parte}'
                )
                ";
        $query = $this->db->query($sql);
        if($query->num_rows > 0){
            return $query->result_array();
        }
        return FALSE;    
    }
    public function addTinta($n_parte, $tintaId)
    {
        $nivel = $this->db->select_max('nivel')->where('n_parte', $n_parte)->get('prod_tintas')->row()->nivel;
        if(is_null($nivel))
            $nivel = 1;
        else
            $nivel++;
        $insert = array(
                    'n_parte' => $n_parte,
                    'nivel' => $nivel,
                    'tinta_id' => $tintaId,
                  );
        $this->db->insert('prod_tintas',$insert);
    }
    
    public function getMaquinas($n_parte)
    {
        $this->db->where('n_parte', $n_parte);
        $this->db->join('maquina', 'maquina.maquina_id = prod_maquina.maquina', 'left');
        $this->db->order_by('orden');
        
        $maquinas = $this->db->get('prod_maquina');
        if($maquinas->num_rows > 0){
            return $maquinas->result_array();
        }
        return false;
    }
    public function getMaquinaNotIn($n_parte)
    {
        $sql = "
                SELECT *
                FROM maquina
                WHERE maquina_id NOT
                IN (
                SELECT maquina.maquina_id
                FROM maquina
                JOIN prod_maquina ON prod_maquina.maquina = maquina.maquina_id
                WHERE n_parte = '{$n_parte}'
                )
                ";
        $query = $this->db->query($sql);
        if($query->num_rows > 0){
            return $query->result_array();
        }
        return FALSE; 
    }
    public function addMaquina($n_parte, $maquina_id)
    {
        $orden = $this->db->select_max('orden')->where('n_parte', $n_parte)->get('prod_maquina')->row()->orden;
        if(is_null($orden))
            $orden = 1;
        else
            $orden++;
        
        $insert = array(
                    'n_parte' => $n_parte,
                    'orden' => $orden,
                    'maquina' => $maquina_id,
                  );
        
        if( $this->db->insert('prod_maquina', $insert) ){
            return true;
        }
        return false;
    }
}
