<?php
$this->load->view('../../themes/default/header');
$this->load->helper('form');
?>

<div class="pagetitle">
    <h1>Productos</h1> <span>Catalogo de productos</span>
</div><!--pagetitle-->

<div class="maincontent">
    <div class="contentinner">

        <h4 class="widgettitle ctitle">Busqueda</h4>
        
        <div class="widgetcontent">  
            <div id="busqueda">
                <?php echo form_open(base_url('buscarnumero')); ?>
                <p>
                    <label>Numero de Parte</label>
                    <span class="field">
                        <input type="text" />
                    </span>
                </p>
                <p class="stdformbutton">
                    <button class="btn btn-primary">Entrar</button>
                </p>

            </div>
        </div><!--widgetcontent-->

        <h4 class="widgettitle ctitle">Lista de Productos</h4>
        <table class="table table-bordered" id="dyntable">
            <colgroup>
                <col class="con0" style="align: center; width: 4%" />
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
            </colgroup>
            <thead>
                <tr>
                    <th><span>ID</span></th>
                    <th><span>Numero Parte</span></th>
                    <th><span>Descripcion</span></th>
                    <th><span>Num Cliente</span></th>
                    <th><span>Resistencia</span></th>
                    <th><span>Flauta</span></th>
                </tr>
            </thead> 
            <tbody>
                <?php foreach ($listaProductos as $productos): ?>
                    <tr>
                        <td><a href="<?php echo base_url() . "productos/view/{$productos['id']}"; ?>" >edit</a></td>
                        <td><?php echo $productos['n_parte'] ?></td>
                        <td><?php echo $productos['descripcion'] ?></td>
                        <td><?php echo $productos['n_cliente'] ?></td>
                        <td><?php echo $productos['resistencia'] ?></td>
                        <td><?php echo $productos['flauta'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <p><?php echo $this->pagination->create_links(); ?></p>

    </div>
</div>
<?php $this->load->view('../../themes/default/fooder'); ?>
