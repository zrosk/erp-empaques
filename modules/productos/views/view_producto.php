<?php $this->load->view('../../themes/default/header'); ?>

<style>
    .sortable-placeholder {
        border: 2px dotted #000;
        background: #DDDDDD;
    }
</style>

<div class="pagetitle">
    <h1><?php echo $item['n_parte'] ?></h1> <span>Informacion</span>
</div><!--pagetitle-->

<div class="maincontent">
    <div class="contentinner">
        <h4 class="widgettitle">Editar</h4>

        <div class="widgetcontent">

            <div class="error">
                <?php echo validation_errors(); ?>
            </div>

            <?php echo form_open(base_url() . 'productos/editar', array('class' => 'stdform')) ?>

            <?php echo form_hidden('id', $item['id']); ?>

            <p>
                <label>Numero de Parte:&nbsp;</label>
                <span class="field">
                    <?php echo form_input('n_parte', $item['n_parte'], 'class="input-medium" readonly') ?>
                </span>

            </p>
            <p>
                <label for="descripcion">Descripcion</label>
                <span class="field">
                    <?php echo form_input('descripcion', $item['descripcion'], 'class="input-medium"') ?>
                </span>

            </p>
            <p>
                <label for="ancho">Ancho</label>
                <span class="field">
                    <?php echo form_input('ancho', $item['ancho'], 'class="input-medium"'); ?>
                </span>
            </p>
            <p>
                <label for="largo">Largo</label>
                <span class="field">
                    <?php echo form_input('largo', $item['largo'], 'class="input-medium"'); ?>
                </span>

            </p>
            <p>
                <label for="resistencia">Resistencia</label>
                <span class="field">
                    <?php echo form_input('resistencia', $item['resistencia'], 'class="input-medium"'); ?>
                </span>

            </p>
            <p>
                <label for="flauta">Flauta</label>
                <span class="field">
                    <?php echo form_dropdown('flauta', array('B' => 'B', 'C' => 'C', 'BC' => 'BC'), $item['flauta'], 'class="input-medium"'); ?>
                </span>

            </p>
            <p>
                <label for="especial">Especial</label>
                <span class="field">
                    <?php echo form_dropdown('especial', array('BLANCO' => 'BLANCO', 'KRAFT' => 'KRAFT'), $item['especial'], 'class="uniformselect"'); ?>
                </span>
            </p>

            <p>
                <label for="marcado">Marcado</label>
                <span class="field">
                    <?php echo form_input('marcado', $item['marcado'], 'class="input-medium"'); ?>
                </span>
            </p>
            <p>
                <label for="cantidad_pliego">Piezas x Pliego</label>
                <span class="field">
                    <?php echo form_input('cantidad_pliego', $item['cantidad_pliego'], 'class="input-medium"'); ?>
                </span>
            </p>
            <p>
                <label for="peso">Peso</label>
                <span class="field">
                    <?php echo form_input('peso', $item['peso'], 'class="input-medium"'); ?>
                </span>
            </p>
            <p>
                <label for="std_pack">std_pack</label>
                <span class="field">
                    <?php echo form_input('std_pack', $item['std_pack'], 'class="input-medium"'); ?>
                </span>
            </p>
            <p>
                <label for="fecha">fecha</label>
                <span class="field">
                    <?php echo form_input('fecha', $item['fecha'], 'class="input-medium"'); ?>
                </span>
            </p>
            <p class="stdformbutton">
                <button class="btn btn-primary">Save</button>
            </p>

            <?php echo form_close(); ?>

        </div><!--widgetcontent-->

        <div class="row-fluid">
            <div class="span6">
                <h4 class="widgettitle">Procesos</h4>
                <div class="widgetcontent">
                    <table class="procesos table table-bordered" id="procesos">
                        <caption>Lista de Procesos</caption>
                        <thead>
                            <tr>
                                <th>Proceso</th>
                            </tr>
                        </thead>
                        <tbody class="sortable">
                            <?php if (!$procesos): ?>
                                <tr id="item_-1">
                                    <td>No existen procesos</td>
                                </tr>
                            <?php else: ?>    
                                <?php
                                foreach ($procesos as $proceso):
                                    $id = $proceso['procesos_id'];
                                    $nombre = $proceso['nombre'];
                                    ?>
                                    <tr id="item_<?= $id ?>" >
                                        <td><?= $nombre ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>    
                        </tbody>
                    </table>
                    <p><input id="add_proce" type="button" value="add" name="add"/></p>
                </div>
            </div>
            <div class="span6">
                <h4 class="widgettitle">Tintas</h4>
                <div class="widgetcontent">
                    <table class="tintas table table-bordered" id="tintas">
                        <caption>Lista de Tintas</caption>
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Descripcion</th>
                            </tr>
                        </thead>
                        <tbody class="sortable">
                            <?php if (!$tintas): ?>
                                <tr id="tinta_-1">
                                    <td colspan="2">No existen tintas</td>
                                </tr>
                                <?php
                            else :
                                foreach ($tintas as $tinta):
                                    $titaId = $tinta['tinta_id'];
                                    $codigo = $tinta['codigo'];
                                    $descripcion = $tinta['descripcion'];
                                    ?>
                                    <tr id="tinta_<?= $titaId ?>" >
                                        <td><?= $codigo ?></td>
                                        <td><?= $descripcion ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>    
                        </tbody>
                    </table>
                    <p><input id="add_tint" type="button" value="Add" name="tintas"/></p>
                </div>
            </div>
        </div>
        <!--- Row 2 -->
        <div class="row-fluid">
            <div class="span6">
                <h4 class="widgettitle">Opciones de MQ en Impresoras</h4>
                <div class="widgetcontent">
                    <table class="maquinas table table-bordered" id="maquinas">
                        <caption>Lista de Maquinas</caption>
                        <thead>
                            <tr>
                                <th>Nombre</th>
                            </tr>
                        </thead>
                        <tbody class="sortable">
                            <?php if (!$maquinas): ?>
                                <tr id="maquina_-1">
                                    <td colspan="2">No existen tintas</td>
                                </tr>
                            <?php else : ?>
                                <?php foreach ($maquinas as $maquina): ?>
                                    <?php $maquinaId = $maquina['maquina_id']; ?>
                                    <?php $nombre = $maquina['nombre']; ?>
                                
                                    <tr id="maquina_<?= $maquinaId ?>" >
                                        <td><?= $nombre ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>    
                        </tbody>
                    </table>
                    <p><input id="add_mq" type="button" value="Add" name="Maquina"/></p> 
                </div>
            </div>
            <div class="span6"></div>
        </div>
        <div class="clearfix"></div>
    </div><!--contentinner-->
</div><!--maincontent-->

<script type="text/javascript">
    jQuery(document).ready(function() {

        var nParte = "<?= $item['n_parte']; ?>";

        $(".sortable").sortable({
            cursor: "move",
            forcePlaceholderSize: true,
            placeholder: "sortable-placeholder",
            update: function() {

                var dataString = $(this).sortable('serialize') + "&n_parte=" + nParte;
                switch ($(this).parent().attr('id')) {
                    case 'procesos':
                        $.post("<?= base_url() ?>productos/setorden", dataString);
                        break;
                    case 'tintas':
                        $.post("<?= base_url()?>productos/setordentint", dataString);
                        break;
                    case 'maquinas':
                        $.post("<?= base_url()?>productos/setordenmq", dataString);
                        break;
                }
            }
        });
                            
        $("#add_proce").click(function() {

            var data = $.get("<?= base_url() ?>productos/getprocesoAddJson/" + nParte, {}, "json");
            data.done(function(json) {
                var op = "";
                for (i in json)
                    op += "<option value =" + json[i]['procesos_id'] + ">" + json[i]['nombre'] + "</option>";

                var select = "<select id='selectProc' class=\"uniformselect\">" + op + "</select>";
                $(".procesos tr:last").after("<tr><td style=\"text-align: center;\" >" + select + "</td></tr>");
                $("#add_proce").before("\
                                <p> \n\
                                <input id=\"save_proce\" type=\"button\" value=\"save\" name=\"save\"/> \n\
                                <input id=\"cansel_proce\" type=\"button\" value=\"cansel\" name=\"cansel\"/> \n\
                                </p>  \
                                ");
                $("#add_proce").prop({disabled: true});

            });

        });//end  .add.click

        $("#save_proce").livequery("click", function() {

            var proc_id = $("#selectProc").val();
            var proc_nom = $("#selectProc option:selected").text();
            var post = $.post("<?= base_url() ?>productos/addproceso/", {n_parte: nParte, proceso_id: proc_id});
            post.done(function() {

                $("#save_proce, #cansel_proce, .procesos tr:last").toggle().remove();
                $("#add_proce").prop({disabled: false});
                var newrow = "<tr id=\"item_" + proc_id + "\">\n\
                              <td>" + proc_nom + "</td>\n\
                              </tr>";
                $(".procesos tr:last").after(newrow);

            }); //end post.done

        }); //end #save.click

        $("#cansel_proce").livequery("click", function() {
            $("#save_proce, #cansel_proce, .procesos tr:last").toggle().remove();
            $("#add_proce").prop({disabled: false});
        }); //end #cansel.click

        //remove Proceso
        $(".procesos tr").dblclick(function() {
            var trNombre = $(this).text().trim();
            if (confirm("Deseas eliminar: " + trNombre)) {
                var trId = $(this).attr('id').split('_');
                trId = parseInt(trId[1]);
                $(this).toggle().remove();
                var dataString = $(".sortable").sortable('serialize') + "&n_parte=" + nParte;
                dataString += "&pro_id=" + trId;
                $.post("<?= base_url() ?>productos/delproc", dataString);
            }
            return false;
        }); //end remove Proceso / tr.dlcick

        //--------------------
        $("#add_tint").click(function() {
            $.getJSON("<?= base_url() ?>productos/getTintasJson/" + nParte, {})
                    .done(function(json) {
                var op = "";
                for (i in json)
                    op += "<option value =" + json[i]['tinta_id'] + ">" +
                            json[i]['nombre'] + " - " + json[i]['descripcion'] +
                            "</option>";
                var select = "<select id='select_tint' class=\"uniformselect\">" + op + "</select>";
                $(".tintas tr:last").after("<tr><td colspan=\"2\" style=\"text-align: center;\" >" + select + "</td></tr>");
                $("#add_tint").prop({disabled: true});
                $("#add_tint").before("\
                                        <p> \n\
                                        <input id=\"save_tint\" type=\"button\" value=\"Save\" name=\"save\"/> \n\
                                        <input id=\"cansel_tint\" type=\"button\" value=\"Cansel\" name=\"cansel\"/> \n\
                                        </p>  \
                    ");
            });
        });

        $("#save_tint").livequery("click", function() {

            var proc_id = $("#select_tint").val();
            var proc_nom = $("#select_tint option:selected").text().split('-');
            var post = $.post("<?= base_url() ?>productos/addtinta/", {n_parte: nParte, tinta_id: proc_id});
            post.done(function() {

                $("#save_tint, #cansel_tint, .tintas tr:last").toggle().remove();
                $("#add_tint").prop({disabled: false});
                var newrow = "<tr id=\"tinta_" + proc_id + "\">\n\
                              <td>" + proc_nom[0] + "</td>\n\
                              <td>" + proc_nom[1] + "</td>\n\
                              ";
                $(".tintas tr:last").after(newrow);

            }); //end post.done

        });

        $("#cansel_tint").livequery("click", function() {
            $("#save_tint, #cansel_tint, .tintas tr:last").toggle().remove();
            $("#add_tint").prop({disabled: false});
        });
        
        $(".tintas tr").livequery("dblclick", function(){
            var dataString;
            var id;
            var trNombre = $(this).text().trim().replace(/ /g,'');
            if (confirm("Deseas eliminar: " + trNombre)) {
                id = $(this).attr("id").split("_");
                id = parseInt(id[1]);
                $(this).toggle().remove();
                dataString = $(".tintas > .sortable").sortable("serialize") + "&n_parte="+ nParte;
                dataString += "&tint_id="+ id;
                $.post("<?= base_url() ?>productos/deltintid", dataString);
                return true;
            }
            return false;
        });

        //--------------------MQ

        $("#add_mq").click(function() {
            var get;
            var op = "";
            var select;

            $("#add_mq").prop({disabled: true});
            get = $.getJSON("<?= base_url() ?>productos/getMaquinasJson/" + nParte, {});

            get.done(function(json) {
                for (i in json)
                    op += "<option value =\"" + json[i]['maquina_id'] + "\" >" + json[i]['nombre'] + "</option>";

                select = "<select id='select_mq' class=\"uniformselect\">" + op + "</select>";
                $(".maquinas tr:last").after(
                        "<tr> \n\
                         <td style=\"text-align: center;\" >" + select + "</td> \n\
                         </tr>"
                        );
                $("#add_mq").before(
                        "<p> \n\
                        <input id=\"save_mq\" type=\"button\" value=\"Save\" name=\"save\"/> \n\
                        <input id=\"cansel_mq\" type=\"button\" value=\"Cansel\" name=\"cansel\"/> \n\
                        </p>"
                        );


            });
        });

        $("#save_mq").livequery("click", function() {
            var maquinaId;
            var maquinaNombre;
            var post;

            maquinaId = $("#select_mq").val();
            maquinaNombre = $("#select_mq option:selected").text();
            post = $.post("<?= base_url() ?>productos/addmaquina/", {n_parte: nParte, maquina_id: maquinaId});
            post.done(function() {
                var newRow;

                $("#save_mq, #cansel_mq, .maquinas tr:last").toggle().remove();

                newRow = "<tr id=\"maquina_" + maquinaId + "\">   \n\
                              <td>" + maquinaNombre + "</td>  \n\
                         </tr>";
                $(".maquinas tr:last").after(newRow);

                $("#add_mq").prop({disabled: false});

            });
        });
        
        $("#cansel_mq").livequery("click", function(){
            $("#save_mq, #cansel_mq, .maquinas tr:last").toggle().remove();
            $("#add_mq").prop({disabled: false});
        });


    });
</script>
<?php $this->load->view('../../themes/default/fooder'); ?>
