<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Productos extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('prod_model');
    }

    public function index($pag = 0)
    {
        $this->load->library("pagination");
        $this->load->helper("url");
        
        $config = array();
        $config["base_url"] = base_url() . '/productos/index';
        $config["total_rows"] = $this->prod_model->countAllProducto();
        $config['per_page'] = 50;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $this->pagination->initialize($config);
    
        $page = (($pag * $config['per_page']) - $config['per_page']) > 0 ? ($pag * $config['per_page']) - $config['per_page'] : 0 ;
        $listaProductos = $this->prod_model->listaProducto($config['per_page'], $page);
          
        $data = array(
            'title' => 'Productos',
            'listaProductos' => $listaProductos,
            'links' => $this->pagination->create_links(),
        );
        
        $this->load->view('index',$data);
    }
    
    
    public function test()
    {
        $this->load->helper("url");
       $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
       print_r($page);
    }
    
    public function view($id)
    {
        if(!$id)
            redirect (base_url(). 'productos/');
              
       /*
        * listaProducto return result_array:  array(array())
        * only take frist elemet
        */
       $where = "id = {$id}"; 
       $item = array_pop($this->prod_model->listaProducto(1,null, $where));
       $procesos_nparte = $this->prod_model->getProcesos($item['n_parte']);
       $tinta_nparte = $this->prod_model->getTintas($item['n_parte']);
       $maquina_nparte = $this->prod_model->getMaquinas($item['n_parte']);
       
       $data['item'] = $item;
       $data['procesos'] = $procesos_nparte;
       $data['tintas'] = $tinta_nparte;
       $data['maquinas'] = $maquina_nparte;
       
       $this->load->view('view_producto',$data);
    }
    
    public function editar()
    {
        $this->form_validation->set_rules('n_parte', 'Numero de Parte', 'required|max_length[9]');
        $this->form_validation->set_rules('descripcion', 'Descripcion','required|trim');
        $this->form_validation->set_rules('ancho', 'Ancho','required|trim|numeric');
        $this->form_validation->set_rules('largo', 'Largo','required|trim|numeric');
        $this->form_validation->set_rules('resistencia', 'Resistencia','required|trim');
        $this->form_validation->set_rules('flauta', 'Flauta','required|trim|max_length[2]');
        $this->form_validation->set_rules('especial', 'especial','required|trim');
        $this->form_validation->set_rules('resistencia', 'Resistencia','required|trim');
        $this->form_validation->set_rules('marcado', 'marcado','required|trim');
        $this->form_validation->set_rules('cantidad_pliego', 'Piezas x Pliego','required|trim|numeric');
        $this->form_validation->set_rules('peso', 'peso','required|trim|numeric');
        $this->form_validation->set_rules('std_pack', 'std_pack','required|trim|numeric');
        $this->form_validation->set_rules('fecha', 'Fecha','required|trim');
        $this->form_validation->set_rules('id', 'Id','required|trim|numeric');
        
        
        $this->form_validation->set_message('required', 'El campo %s es obligatorio');
        $this->form_validation->set_message('numeric','El campo %s solo admite numeros');
        $this->form_validation->set_message('max_length','Formato no valido en %s');
        
        
        if($this->form_validation->run() === FALSE){
            $this->view($this->input->post('id'));
        }
        else {
            $update = array(
                'descripcion' => $this->input->post('descripcion'),
                'ancho' => $this->input->post('ancho'),
                'largo' => $this->input->post('largo'),
                'resistencia' => $this->input->post('resistencia'),
                'flauta' => $this->input->post('flauta'),
                'especial' => $this->input->post('especial'),
                'resistencia' => $this->input->post('resistencia'),
                'marcado' => $this->input->post('marcado'),
                'cantidad_pliego' => $this->input->post('cantidad_pliego'),
                'peso' => $this->input->post('peso'),
                'std_pack' => $this->input->post('std_pack'),
                'fecha' => $this->input->post('fecha'),
                'id' => $this->input->post('id'),
            );
            
            if(!$this->prod_model->updateProducto($update))
                $data['error'] = 'Error al actualizar';
            redirect(base_url().'/productos');
            
        }
    } 
    public function getprocesoAddJson($n_parte)
    {

        if ($this->input->is_ajax_request()) {

            $json = $this->prod_model->getProcesosNotIn($n_parte);
            $this->output->set_header("Content-Type: application/json; charset=utf-8");
            $this->output
            ->set_content_type('application/json')
            ->set_output($json);
        }
    }
    public function addProceso()
    {
        $n_parte = $this->input->post('n_parte');
        $procesoID = $this->input->post('proceso_id');
        
        $this->prod_model->addProceso($n_parte,$procesoID);
    }
    
    public function setOrden()
    {
        $n_parte = $this->input->post('n_parte');
        $orden = $this->input->post('item');
        
        foreach ($orden as $key => $value) {
            $this->db->set('orden', $key);
            $this->db->where('n_parte', $n_parte)->where('procesos_id', $value);
            $this->db->update('prod_procesos');
            
        }
    }
    public function delproc()
    {
        $n_parte = $this->input->post('n_parte');
        $pro_id = $this->input->post('pro_id');
        $orden = $this->input->post('item');
        
        $this->db->where('n_parte', $n_parte);
        $this->db->where('procesos_id', $pro_id);
        $this->db->delete('prod_procesos');
        
        foreach ($orden as $key => $value) {
            $this->db->set('orden', $key);
            $this->db->where('n_parte', $n_parte)->where('procesos_id', $value);
            $this->db->update('prod_procesos');
            
        }
    }
    
    public function getTintasJson($n_parte)
    {
        if ($this->input->is_ajax_request()) {
            $json = $this->prod_model->getTintaNotIn($n_parte);
            $this->output->set_header("Content-Type: application/json; charset=utf-8");
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
        }
        return false;
    }
    public function addTinta()
    {
        $n_parte = $this->input->post('n_parte');
        $tintaID = $this->input->post('tinta_id');
        
        $this->prod_model->addTinta($n_parte,$tintaID);
    }
    public function setOrdenTint()
    {
        $n_parte = $this->input->post('n_parte');
        $orden = $this->input->post('tinta');
        
        foreach ($orden as $key => $value) {
            $this->db->set('nivel', $key);
            $this->db->where('n_parte', $n_parte)->where('tinta_id', $value);
            $this->db->update('prod_tintas');
            
        }
    }
    public function delTintId()
    {
        $n_parte = $this->input->post('n_parte');
        $tintaID = $this->input->post('tint_id');
        $orden = $this->input->post('tinta');
        
        $this->db->where('n_parte', $n_parte)->where('tinta_id', $tintaID);
        $this->db->delete('prod_tintas');
        
        foreach ($orden as $key => $value) {
            $this->db->set('nivel', $key);
            $this->db->where('n_parte', $n_parte)->where('tinta_id', $value);
            $this->db->update('prod_tintas');
            
        }
        
    }
    public function getMaquinasJson($n_parte)
    {
        if ($this->input->is_ajax_request()) {
            
            $json = $this->prod_model->getMaquinaNotIn($n_parte);
            $this->output->set_header("Content-Type: application/json; charset=utf-8");
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
        }
        return false;
    }
    public function addmaquina()
    {
        $n_parte = $this->input->post('n_parte');
        $maquinaId = $this->input->post('maquina_id');
        
        if( $this->prod_model->addMaquina($n_parte, $maquinaId) ){
            echo 'true';
            return true;
        }
        return false;
        
    }

}
