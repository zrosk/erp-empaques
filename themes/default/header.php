<?php
define('BASETHEME', $this->config->base_url() . 'themes/default/');
?>

<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Proyecto</title>
    <link rel="stylesheet" href="<?php echo BASETHEME; ?>css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo BASETHEME; ?>prettify/prettify.css" type="text/css" />


    <script type="text/javascript" src="<?php echo BASETHEME; ?>prettify/prettify.js"></script>
    <script type="text/javascript" src="<?php echo BASETHEME; ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo BASETHEME; ?>js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo BASETHEME; ?>js/jquery-ui-1.10.3.min.js"></script>
    <script type="text/javascript" src="<?php echo BASETHEME; ?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo BASETHEME; ?>js/colorpicker.js"></script>
    <script type="text/javascript" src="<?php echo BASETHEME; ?>js/jquery.jgrowl.js"></script>
    <script type="text/javascript" src="<?php echo BASETHEME; ?>js/jquery.alerts.js"></script>
    <script type="text/javascript" src="<?php echo BASETHEME; ?>js/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo BASETHEME; ?>js/slider.js"></script>
    <script type="text/javascript" src="<?php echo BASETHEME; ?>js/jquery.livequery.js"></script>


<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="<?php echo BASETHEME; ?>js/excanvas.min.js"></script><![endif]-->
</head>

<body>

    <div class="mainwrapper">
        <?php $this->load->view('../../themes/default/left'); ?>

        <!-- START OF RIGHT PANEL -->
        <div class="rightpanel">
            <div class="headerpanel">
                <a href="" class="showmenu"></a>

                <div class="headerright">
                    <div class="dropdown notification">
                        <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="/page.html">
                            <span class="iconsweets-globe iconsweets-white"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="nav-header">Notifications</li>
                            <li>
                                <a href="">
                                    <strong>3 people viewed your profile</strong><br />
                                    <img src="<?php echo BASETHEME; ?>img/thumbs/thumb1.png" alt="" />
                                    <img src="<?php echo BASETHEME; ?>img/thumbs/thumb2.png" alt="" />
                                    <img src="<?php echo BASETHEME; ?>img/thumbs/thumb3.png" alt="" />
                                </a>
                            </li>
                            <li><a href=""><span class="icon-envelope"></span> New message from <strong>Jack</strong> <small class="muted"> - 19 hours ago</small></a></li>
                            <li><a href=""><span class="icon-envelope"></span> New message from <strong>Daniel</strong> <small class="muted"> - 2 days ago</small></a></li>
                            <li><a href=""><span class="icon-user"></span> <strong>Bruce</strong> is now following you <small class="muted"> - 2 days ago</small></a></li>
                            <li class="viewmore"><a href="">View More Notifications</a></li>
                        </ul>
                    </div><!--dropdown-->

                    <div class="dropdown userinfo">
                        <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="/page.html">Hi, ThemePixels! <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="editprofile.html"><span class="icon-edit"></span> Edit Profile</a></li>
                            <li><a href=""><span class="icon-wrench"></span> Account Settings</a></li>
                            <li><a href=""><span class="icon-eye-open"></span> Privacy Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="index.html"><span class="icon-off"></span> Sign Out</a></li>
                        </ul>
                    </div><!--dropdown-->

                </div><!--headerright-->

            </div><!--headerpanel-->
            <div class="breadcrumbwidget">
                <ul class="skins">
                    <li><a href="default" class="skin-color default"></a></li>
                    <li><a href="orange" class="skin-color orange"></a></li>
                    <li><a href="dark" class="skin-color dark"></a></li>
                    <li>&nbsp;</li>
                    <li class="fixed"><a href="" class="skin-layout fixed"></a></li>
                    <li class="wide"><a href="" class="skin-layout wide"></a></li>
                </ul><!--skins-->
                <ul class="breadcrumb">
                    <li><a href="dashboard.html">Home</a> <span class="divider">/</span></li>
                    <li class="active">Dashboard</li>
                </ul>
            </div><!--breadcrumbwidget-->
