<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . "/libraries/lib_excel/PHPExcel/IOFactory.php";

class Excel extends PHPExcel_IOFactory
{

    public function __construct()
    {
        //parent::__construct();
    }

}
class MyReadFilter implements PHPExcel_Reader_IReadFilter
{
	public function readCell($column, $row, $worksheetName = '') {
		// Read rows 1 to 7 and columns A to E only
		if ($row >= 1 && $row <= 7) {
			if (in_array($column,range('A','E'))) {
				return true;
			}
		}
		return false;
	}
}


/* End of file */
    /* Location: ./application/libraries/ */
	

