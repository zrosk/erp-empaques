<!DOCTYPE HTML>
<html>
    <head>
        <title><?= $titulo; ?></title>
    </head>
    
    <body>  
        <h1>Mi Formulario</h1>
        
        <?= form_open(base_url().'index.php/formulario/validar',array('name'=>'mi_form','id'=>'form'));?>
        <?= form_label('Nombre','Nombre',array('class'=>'label'));?><br />
        <?= form_input('nombre',@set_value('nombre'),'class="input"')?><br />
        <?= form_input('correo',@set_value('correo'),'class="input"')?><br />
        <?= form_password('pass',@set_value('pass'),'class="password"');?> <br />
        Futbol<?= form_checkbox('deporte[]','futbol',false,'class="check" '.@set_checkbox('deporte[]','futbol'));?>
        Voley<?= form_checkbox('deporte[]','voley',true,'class="check"' .@set_checkbox('deporte[]','voley'));?><br />
        <?= form_textarea('mensaje',@set_value('mensaje'),'class="textarea" row="25px"');?><br />
        Masculino<?= form_radio('sexo','M',false,'class="radio" '.@set_radio('sexo','M'));?>
        Femenino<?= form_radio('sexo','F',false,'class="radio" '.@set_radio('sexo','F'));?><br />
        <?= form_button('boton','Titulo Boton','class="boton"');?><br />
        <?= form_hidden('oculto','valor_oculto');?><br />
        <?= form_upload('archivo','','class="class_upload"');?><br />
        <?php /* form_dropdown('seleccione',array('1'=>'uno','2'=>'dos','3'=>'tre'),array('2'),'class="select"');?><br />
        <?= form_multiselect('seleccione2[]',array('1'=>'uno','2'=>'dos','3'=>'tre'),array('1','2'),'class="select"'); */?><br />
        <select name="seleccione">
            <option value="1" <?= @set_select('seleccione','1');?>>uno</option>
            <option value="2" <?= @set_select('seleccione','2');?>>dos</option>
            <option value="3" <?= @set_select('seleccione','3');?>>tres</option>
        </select><br />
        <select name="seleccione2[]" multiple="">
            <option value="1" <?= @set_select('seleccione2[]','1');?>>uno</option>
            <option value="2" <?= @set_select('seleccione2[]','2');?>>dos</option>
            <option value="3" <?= @set_select('seleccione2[]','3');?>>tres</option>
        </select>
        <?= form_submit('submit','Enviar Datos','class="submit"');?>
        <?= form_close();?>
        
        <hr />
        
        <h3>Posibles Errores</h3>
        <?= validation_errors();?>
    </body>
</html>
