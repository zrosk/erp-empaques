<!DOCTYPE HTML>
<html>
    <head>
        <title><?= $titulo;?></title>
        <meta charset="utf-8" />
        
        <!--<link href="<?= base_url();?>plantilla/front/css/estilos.css" rel="stylesheet" />
        <script type="text/javascript" src="<?= base_url();?>plantilla/front/js/funciones.js"></script>-->
    </head>
    
    <body onload="saluda();">
        <?php if(isset($error)): ?>
            <?= $error; ?>
        <?php else: ?>
        <h1>Modificar Noticia</h1>
        <form action="<?= base_url();?>consultas/edit_noticia" name="form" method="POST">
            Titulo:<input type="text" name="titulo" value="<?= $titulo_notica;?>" /><br />
            Cuerpo:<input type="text" name="cuerpo" value="<?= $cuerpo_notica;?>" /><br />
            <input type="hidden" name="id" value="<?= $id_notica;?>" />
            <input type="submit" value="Modificar" name="submit" />
            <hr />
        </form>
        <?php endif; ?>
    </body>
</html>