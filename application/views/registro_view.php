<html>
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    
    <body>
        <h1>Registrar Usuario</h1>
        
        <form name="form_reg" action="<?= base_url().'usuarios/registro_very'?>" method="POST" enctype="multipart/form-data" >
        <table>
            <tr>
                <td>Nombre</td>
                <td><input type="text" name="nombre" value="<?= @set_value('nombre')?>" /></td>
            </tr>
            <tr>
                <td>Apellidos</td>
                <td><input type="text" name="apellidos" value="<?= @set_value('apellidos')?>" /></td>
            </tr>
            <tr>
                <td>E-Mail</td>
                <td><input type="text" name="email" value="<?= @set_value('email')?>" /></td>
            </tr>
            <tr>
                <td>Contraseña</td>
                <td><input type="password" name="pass" value="<?= @set_value('pass')?>" /></td>
            </tr>
            <tr>
                <td>Confirmar Contraseña</td>
                <td><input type="password" name="repass" value="<?= @set_value('repass')?>" /></td>
            </tr>
            <tr>
                <td>Avatar</td>
                <td><input type="file" name="userfile" value="<?= @set_value('userfile')?>" /></td>
            </tr>
            <tr>
                <td><input type="submit" value="Registrar" name="submit_reg" /></td>
                <td><a href="<?= base_url().'usuarios/'?>" title="Inicar Sesión">Inicar Sesión</a></td>
            </tr>
        </table>
   
        </form>
        <hr />
        <?php if(isset($mensaje)):?>
        <?php if(is_array($mensaje)):?>
        <?php foreach($mensaje as $m):?>
        <?php echo $m;?>
        <?php endforeach;?>
        <?php else:?>
        <?php echo $mensaje;?>
        <?php endif;?>
        <?php endif;?>
        
        <?= validation_errors();?>
    </body>
</html>
