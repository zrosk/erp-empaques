<!DOCTYPE HTML>
<html>
    <head>
        <title><?= $titulo;?></title>
        <meta charset="utf-8" />
        
        <!--<link href="<?= base_url();?>plantilla/front/css/estilos.css" rel="stylesheet" />
        <script type="text/javascript" src="<?= base_url();?>plantilla/front/js/funciones.js"></script>-->
    </head>
    
    <body onload="saluda();">
        <h1>INSERTAR NOTICIA</h1>
        <form action="<?= base_url();?>consultas/add_noticia" name="form" method="POST">
            Titulo:<input type="text" name="titulo" /><br />
            Cuerpo:<input type="text" name="cuerpo" /><br />
            <input type="submit" value="Enviar" name="submit" />
            <hr />
        </form>
        <h1>Mis Noticias</h1>
        <?php foreach($noticias as $fila):?>
        <h1><?= $fila->titulo; ?></h1>
        <p><?= $fila->cuerpo; ?></p><a href="<?= base_url()?>consultas/edit_noticia/<?= $fila->id;?>">Editar</a> | 
        <a href="<?= base_url()?>consultas/delete_noticia/<?= $fila->id;?>">Eliminar</a>
        <br />
        <?php endforeach; ?>
    </body>
</html>