<html>
    <head>
        <title>Ejemplo De Ajax</title>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
               $("#registrar").click(function(){
                    //alert('hola');
                    $.ajax({
                       url: '<?= base_url();?>' + 'ajax/agregar',
                       type: 'POST',
                       data: $('#form').serialize(),
                       success: function(msj){
                            //alert(msj);
                            $("#mensaje").html(msj);
                            if(msj == 'Registro Completado')
                            {
                                jQuery.fn.reset = function () {
                                  $(this).each (function() { this.reset(); });
                                }
                                $("#form").reset();
                            }
                       }
                    });
                    return false;
               }) 
            });
        </script>
    </head>
    
    <body>
        <form id="form">
        <table>
            <tr>
                <td>Nombre</td>
                <td><input type="text" name="name" id="name" /></td>
            </tr>
            <tr>
                <td>Apellidos</td>
                <td><input type="text" name="apellidos" id="apellidos" /></td>
            </tr>
            <tr>
                <td>Correo</td>
                <td><input type="text" name="correo" id="correo" /></td>
            </tr>
            <tr>
                <td>Descripcion</td>
                <td><textarea rows="5" cols="15" name="descripcion" id="descripcion"></textarea></td>
            </tr>
            <tr>
                <td>Sexo</td>
                <td>M<input type="radio" name="sexo" id="sexo" value="M" /> &nbsp;&nbsp;
                F<input type="radio" name="sexo" id="sexo" value="F" /></td>
            </tr>
            <tr>
                <td>Deporte</td>
                <td>Futbol<input type="checkbox" name="deporte[]" id="deporte[]" value="Futbol" /> &nbsp;&nbsp;
                Voley<input type="checkbox" name="deporte[]" id="deporte[]" value="Voley" /></td>
            </tr>
            <tr>
                <td>Color</td>
                <td>
                    <select name="color" id="color">
                        <option value="Amarillo">Amarillo</option>
                        <option value="Rojo">Rojo</option>
                        <option value="Azul">Azul</option>
                        <option value="Negro">Negro</option>
                        <option value="Verde">Verde</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Numero</td>
                <td>
                    <select name="numero[]" id="numero[]" multiple="">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><input type="button" value="Registrar" id="registrar" /></td>
            </tr>
        </table>
        </form>
        <div id="mensaje"></div>
    </body>
</html>