<?php

class Consultas_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_noticias()
    {
        //$consulta = $this->db->query('Select * from noticias');
        $consulta = $this->db->get('noticias');
        
        return $consulta->result();
    }
    
    public function add_noticia()
    {
        //$this->db->query('Insert into noticias values(null,"'.$this->input->post('titulo').'","'.$this->input->post('cuerpo').'",1)');
        $data = array(
                'titulo' => $this->input->post('titulo'),
                'cuerpo' => $this->input->post('cuerpo'),
                'id_usuario' => 1
                );
        $this->db->insert('noticias',$data);
    }
    
    public function get_noticia_id($id)
    {
        $consulta = $this->db->get_where('noticias',array('id' => $id));
        if($consulta->num_rows() > 0)
        {
            return $consulta->row_array();
        }
        else
        {
            return false;
        }
    }
    
    public function edit_noticia()
    {
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('noticias',array(
                                        'titulo' => $this->input->post('titulo'),
                                        'cuerpo' => $this->input->post('cuerpo')
                                        ));
    }
    
    public function delete_noticia($id)
    {
        $this->db->delete('noticias',array('id'=>$id));
    }
    
    public function agregar()
    {
        $deporte = '';
        foreach($this->input->post('deporte',TRUE) as $de)
        {
            $deporte .= $de.',';
        }
        
        $numero = '';
        foreach($this->input->post('numero',TRUE) as $num)
        {
            $numero .= $num.',';
        }
        
        $this->db->insert('registro',array(
        
                'nombre'=>$this->input->post('name',TRUE),
                'apellidos'=>$this->input->post('apellidos',TRUE),
                'correo'=>$this->input->post('correo',TRUE),
                'descripcion'=>$this->input->post('descripcion',TRUE),
                'sexo'=>$this->input->post('sexo',TRUE),
                'deporte'=>$deporte,
                'color'=>$this->input->post('color',TRUE),
                'numero'=>$numero
        ));
    }
}