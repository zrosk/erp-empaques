<?php
class Usuarios_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function very($variable,$campo)
    {
        $consulta = $this->db->get_where('usuarios',array($campo=>$variable));
        if($consulta->num_rows() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function very_estado()
    {
        $consulta =$this->db->get_where('usuarios',array(
                                    'email'=>$this->input->post('email',TRUE),
                                    'pass' => $this->input->post('pass',TRUE),
                                    'estado' => '1'));
        if($consulta->num_rows() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function add_user($imagen)
    {
        $code = rand(1000,99999);
        $this->db->insert('usuarios',array(
                            'nombre'=>$this->input->post('nombre',TRUE),
                            'apellidos'=>$this->input->post('apellidos',TRUE),
                            'email'=>$this->input->post('email',TRUE),
                            'pass'=>$this->input->post('pass',TRUE),
                            'codigo'=>$code,
                            'estado'=>'0',
                            'nivel'=>'Registrado',
                            'foto' => $imagen,
                            'fecha_creacion' => date('Y-m-d H:i:s')
                            ));
        //Envio De Email
        $this->load->library('email');

        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html';
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['smtp_port'] = 465;
        $config['smtp_user'] = 'renattonl@gmail.com';
        $config['smtp_pass'] = '123456789@a';
        $config['validation'] = TRUE; 
        $this->email->initialize($config);
        $this->email->clear();

        $this->email->from('renattonl@gmail.com','Renato NL');
        $this->email->to($this->input->post('email',TRUE));
        //$this->email->cc('email.alternativo'); 
        $this->email->subject('Confirme Cuenta De Usuario - Curso CI');
        $this->email->message('<h1>Bienvenido: '.$this->input->post('nombre',TRUE).' '.$this->input->post('apellidos',TRUE).'</h1>
                            <p>Para confirmar su registro ingrese a la siguiente direccion:
                            <a href="'.base_url().'usuarios/confirmar/'.$code.'">Confirmar Registro</a>
                            <b>Gracias Por Su Registro</b>
                            </p>');
        $this->email->send(); 

    }
    
    public function very_sesion()
    {
        $consulta =$this->db->get_where('usuarios',array(
                                    'email'=>$this->input->post('email',TRUE),
                                    'pass' => $this->input->post('pass',TRUE)));
        if($consulta->num_rows() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function update_user($code)
    {
        $this->db->where('codigo',$code);
        $this->db->update('usuarios',array('estado'=>'1'));
    }
}