<?php

class Formulario extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }
    
    public function index()
    {
        
        $data = array(
            'titulo' => 'Formulario'
        );
        $this->load->view('formulario_view',$data);
    }
    
    public function validar()
    {
        $this->form_validation->set_rules('nombre', 'Nombre Usuario', 'required|trim');
        $this->form_validation->set_rules('pass', 'Contraseña Usuario', 'required|trim');
        $this->form_validation->set_rules('deporte[]','Deporte Favorito','required|trim|callback_valid_deporte|callback_num_deporte');
        $this->form_validation->set_rules('correo','E-Mail','required|valid_email');
        $this->form_validation->set_rules('mensaje','Mensaje','required|min_length[10]|max_length[20]');
        $this->form_validation->set_rules('sexo','Sexo','required|trim|callback_valid_sexo');
        $this->form_validation->set_rules('archivo','Archivo','required|trim');
        $this->form_validation->set_rules('seleccione','Seleccione 1','required|trim|callback_valid_select1');
        $this->form_validation->set_rules('seleccione2[]','Seleccione 2','required|trim|callback_valid_select2');
        
        $this->form_validation->set_message('valid_select2','El Select2 seleccionado NO EXISTE');
        $this->form_validation->set_message('valid_select1','El Select1 seleccionado NO EXISTE');
        $this->form_validation->set_message('valid_sexo','El Sexo Seleccionado NO EXISTE');
        $this->form_validation->set_message('num_deporte','Seleccione al menos 2 deportes');
        $this->form_validation->set_message('valid_deporte','El Deporte Seleccione NO EXISTE');
        $this->form_validation->set_message('valid_email','El E-Mail %s Es Invalido');
        $this->form_validation->set_message('required','El Campo %s Es Obligatorio');
        $this->form_validation->set_message('max_length','El Campo %s Tiene que como maximo 20 caracteres');
        $this->form_validation->set_message('min_length','El Campo %s Tiene que como minimo 10 caracteres');
        if ($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            //echo "Todos los datos estan OK: ".$this->input->post('pass');
            $data = array(
                    'nombre'=>$this->input->post('nombre'),
                    'correo'=>$this->input->post('correo'),
                    'contrasena'=>$this->input->post('pass'),
                    'deporte'=>$this->input->post('deporte'),
                    'mensaje'=>$this->input->post('mensaje'),
                    'sexo'=>$this->input->post('sexo'),
                    'archivo'=>$this->input->post('archivo'),
                    'seleccione'=>$this->input->post('seleccione'),
                    'seleccione2'=>$this->input->post('seleccione2')
                    );
            $this->load->view('formulario_datos',$data);
        }
    }
    
    function valid_deporte($deporte)
    {
        $deporte = array($deporte);
        $con = 0;
        foreach($deporte as $d)
        {
            switch($d)
            {
                case 'futbol':
                    $con++;
                    break;
                case 'voley':
                    $con++;
                    break;
                default:
                    $con--;
                    break;
            }
        }
        
        if($con == count($deporte))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function num_deporte()
    {
        $deporte = $this->input->post('deporte');
        if(count($deporte) > 2)
        {
            return false;
        }
        if(count($deporte)<2)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    function valid_sexo($sexo)
    {
        switch($sexo)
        {
            case 'M':
                return true;
                break;
            case 'F':
                return true;
                break;
            default:
                return false;
                break;
        }
    }
    
    function valid_select1($select)
    {
        switch($select)
        {
            case '1':
                return true;
                break;
            case '2':
                return true;
                break;
            case '3':
                return true;
                break;
            default:
                return false;
                break;
        }
    }
    
    function valid_select2($select)
    {
        $select = array($select);
        $con = 0;
        foreach($select as $d)
        {
            switch($d)
            {
                case '1':
                    $con++;
                    break;
                case '2':
                    $con++;
                    break;
                case '3':
                    $con++;
                    break;
                default:
                    return false;
                    break;
            }
        }
        
        if($con == count($select))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}