<?php

class Panel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->very_sesion();
    }
    
    public function index()
    {
        echo "Hola usuario: ".$this->session->userdata('usuario').
                "<br> <a href='".base_url()."panel/cerrar'>Cerrar Sesion</a>";
    }
    
    function very_sesion()
    {
        if(!$this->session->userdata('usuario'))
        {
            redirect(base_url().'usuarios');
        }
    }
    
    public function cerrar()
    {
        $this->session->sess_destroy();
        redirect(base_url().'usuarios');
    }
}