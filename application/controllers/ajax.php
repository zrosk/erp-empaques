<?php

class Ajax extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('consultas_model');
    }
    
    public function index()
    {
        $this->load->view('ajax_view');
    }
    
    public function agregar()
    {
        if(!$this->input->is_ajax_request())
        {
            redirect('404');
        }
        else
        {
            //echo "hola";
            $this->form_validation->set_rules('name','Nombre','required');
            $this->form_validation->set_rules('apellidos','Apellidos','required|xss_clean');
            $this->form_validation->set_rules('correo','correo','required|valid_email|xss_clean');
            $this->form_validation->set_rules('descripcion','Descripcion','required|xss_clean');
            $this->form_validation->set_rules('sexo','Sexo','required|xss_clean');
            $this->form_validation->set_rules('deporte','Deporte','required|xss_clean');
            $this->form_validation->set_rules('color','Color','required|xss_clean');
            $this->form_validation->set_rules('numero','Numero','required|xss_clean');
            
            $this->form_validation->set_message('required','El Campo %s es requerido ');
            $this->form_validation->set_message('valid_email','El Campo %s es Invalido ');
            
            if($this->form_validation->run() == false)
            {
                $error = json_encode(validation_errors());
                $error = str_replace('"',"",$error);
                $error = str_replace('<\/p>\n',"",$error);
                echo $error;
            }
            else
            {
                //
                $this->consultas_model->agregar();
                echo "Registro Completado";
            }
        }
    }
}