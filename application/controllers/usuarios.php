<?php

class Usuarios extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('usuarios_model');
        $this->very_login();
    }
    
    public function index()
    {
        $this->load->view('usuarios_view');
    }
    
    public function registro()
    {
        $this->load->view('registro_view');
    }
    
    public function registro_very()
    {
        if($this->input->post('submit_reg'))
        {
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('apellidos','Apellidos','required');
            $this->form_validation->set_rules('email','E-Mail','required|trim|valid_email|callback_very_correo');
            $this->form_validation->set_rules('pass','Contraseña','required|trim|min_length[6]');
            $this->form_validation->set_rules('repass','Confirme Contraseña','required|trim|matches[pass]');
            
            $this->form_validation->set_message('required','El Campo %s Es Obligatorio');
            $this->form_validation->set_message('valid_email','Ingrese un %s Válido');
            $this->form_validation->set_message('matches','El Campo %s no es igual que el campos %s');
            $this->form_validation->set_message('min_length','El Campo %s debe tener como minimo 6 caracteres');
            $this->form_validation->set_message('very_correo','El Campo %s Ya Existe');

            if($this->form_validation->run() != FALSE)
            {
                if($_FILES['userfile']['name'] != '')
                {
                    $respuesta = $this->upload_image();
                    
                    if(!is_array($respuesta))
                    {
                        $this->usuarios_model->add_user($respuesta);
                        $mensaje = "El Usuario Se Registro Correctamente";
                    }
                    else
                    {
                        $mensaje = $respuesta;
                    }
                }
                else
                {
                    $this->usuarios_model->add_user('anonimo.jpg');
                    $mensaje = "El Usuario Se Registro Correctamente";
                }
                
                
                $data = array('mensaje'=>$mensaje);
                $this->load->view('registro_view',$data);
            }
            else
            {
                $this->load->view('registro_view');
            }
        }
        else
        {
            redirect(base_url().'usuarios/registro');
        }
    }
    
    function very_correo($correo)
    {
        $variable = $this->usuarios_model->very($correo,'email');
        if($variable == true)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public function very_sesion()
    {
        if($this->input->post('submit'))
        {
            $variable = $this->usuarios_model->very_sesion();
            if($variable == true)
            {
                $estado = $this->usuarios_model->very_estado();
                if($estado == true)
                {
                    $variables = array(
                                'usuario' => $this->input->post('email')
                            );
                    $this->session->set_userdata($variables);
                    redirect(base_url().'panel');
                }
                else
                {
                    $data = array('mensaje' => 'El usuario no esta activo | verifique su correo');
                
                    $this->load->view('usuarios_view',$data);
                }
            }
            else
            {
                $data = array('mensaje' => 'El E-Mail/Contraseña Son Incorrectos');
                
                $this->load->view('usuarios_view',$data);
            }
        }
        else
        {
            redirect(base_url().'usuarios');
        }
    }
    
    function upload_image()
    {
        $config['upload_path'] = 'files/imagenes/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= 2*1024;
		$config['max_width']  = '1024';
		$config['max_height']  = '1024';
        $config['file_name']  = $this->input->post('nombre').$this->input->post('apellidos');
        $config['remove_spaces'] = TRUE;
		
		$this->load->library('upload', $config);
	
		if ( !$this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			return $error;
		}	
		else
		{
		    $data = $this->upload->data(); 
			$this->create_thumb($data['file_name']);
			
			return $data['file_name'];
		}
    }
    
    function create_thumb($imagen)
    {
        $config['image_library'] = 'gd2';
        $config['source_image']	= 'files/imagenes/'.$imagen;
        $config['new_image']	= 'files/imagenes/thumbs/';
        $config['thumb_marker']	= '';
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width']	 = 150;
        $config['height']	= 150;
        
        $this->load->library('image_lib', $config); 
        $this->image_lib->resize();
    }
    
    public function confirmar($code)
    {
        $res = $this->usuarios_model->very($code,'codigo');
        if($res == false)
        {
            echo "Este Usuario No Existe";
        }
        else
        {
            $this->usuarios_model->update_user($code);
            echo "Usuario Confirmado Con Existo.<br>
                <a href='".base_url()."usuarios'>Inciar Sesion</a>";
        }
    }
    
    public function very_login()
    {
        if($this->session->userdata('usuario'))
        {
            redirect(base_url()."panel");
        }
    }
}