<?php

class Inicio extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        //echo "Hola desde el controlador";
        $this->load->view('inicio_view');
    }
    
    public function index2($parametro1 = false,$parametro2 = '')
    {
        echo "hola desde el index2 ".$parametro1.' '.$parametro2;
    }
}