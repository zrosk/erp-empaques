<?php

class Consultas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('consultas_model');
    }
    
    public function index()
    {
        $data = array(
                    'titulo' => 'Mis Consultas',
                    'noticias' => $this->consultas_model->get_noticias()
                    );
        $this->load->view('constulas_view',$data);
    }
    
    public function add_noticia()
    {
        if($this->input->post('submit'))
        {
            $this->consultas_model->add_noticia();
            redirect(base_url().'consultas');
        }
    }
    
    public function edit_noticia($id)
    {
        $verificar = $this->consultas_model->get_noticia_id($id);
        
        if($this->input->post('submit'))
        {
            $this->consultas_model->edit_noticia();
            redirect(base_url().'consultas');
        }
        
        if($verificar == false)
        {
            $data = array(
                    'titulo' => 'Editar Noticia',
                    'error' => 'Esta Noticia No Existe'
                    );
            
        }
        else
        {
            $data = array(
                    'titulo' => 'Editar Noticia',
                    'titulo_notica' => $verificar['titulo'],
                    'cuerpo_notica' => $verificar['cuerpo'],
                    'id_notica' => $verificar['id']
                    );
        }
        $this->load->view('edit_noticia_view',$data);
    }
    
    public function delete_noticia($id)
    {
        $verificar = $this->consultas_model->get_noticia_id($id);
        
        if($verificar == false)
        {
            echo "La Noticia No Existe";            
        }
        else
        {
            $this->consultas_model->delete_noticia($id);
            redirect(base_url().'consultas');
        }
    }
}